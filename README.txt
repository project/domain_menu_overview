README.txt
==========

WHAT DOES DOMAIN MENU OVERVIEW MODULE DO?
=============================

This module enables you to have an clear overview per domain what the menu
entries are. In Drupal all of these links are on the same page and thus
it is not clear in which domain it is and the menus will grow
very big in time aswell.

You will be able to select which menus will be editable by this module.
(configuration part)

After you have selected which menus you want to be editable, these menus
will be visible separated by domain.

This will provide a better overview for customers with bigger menus or
for a multi domain approach.

at admin/structure/
for example : admin/structure/main-menu-primary_domain

Features
==================
Select which menu are being used by this module
menus per domain

Requirements
=======================
Drupal 7
Domain module
